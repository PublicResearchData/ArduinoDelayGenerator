README
======

Code for arduino delay generator assuming output from a Jasper JD8714 and input to a ThorLabs Kiralux camera (e.g. CS126MU) with TSI-IOBOB2 breakout board and 8050-CAB1 cable. 

## Contributors

### Peter J. Christopher
* Email: pjc209@cam.ac.uk
* Email: peterjchristopher@gmail.com
* LinkedIn: https://www.linkedin.com/in/peterjchristopher/
* Website: www.peterjchristopher.me.uk
* Blog: http://peterjchristopher.blogspot.com/
* Address: Centre of Molecular Materials for Photonics and Electronics (CMMPE), Centre for Advanced Photonics and Electronics, University of Cambridge, 9 JJ Thomson Avenue, Cambridge, UK, CB3 0FF

