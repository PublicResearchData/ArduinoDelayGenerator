int cameraOut = 3; // Corresponds to TRIGIN
int displayIn = 6; // Corresponds to TRIGOUT
int prevDisplayIn; // Previous value of displayIn pin
int delayTime = 15500; // [us]
int pulseLength = 1000; // [us]

// We use the TRIGOUT channel to read 3.3V rising edge pulses from the SLM. 
// We use the TRIGIN channel to write 5V pulses to the camera.
// We delay by X ms the SLM input (on TRIGOUT) and output to camera (ON TRIGIN)
void setup()
{
  pinMode(cameraOut, OUTPUT);
  pinMode(displayIn, INPUT);
  digitalWrite(cameraOut, LOW);
  prevDisplayIn=LOW;
}

void loop()
{
  while(1)
  {
    int newDisplayIn=digitalRead(displayIn);
    if(newDisplayIn == HIGH and prevDisplayIn == LOW)
    {
      delayMicroseconds(delayTime);
      digitalWrite(cameraOut, HIGH);
      delayMicroseconds(pulseLength);
      digitalWrite(cameraOut, LOW);
    }
    prevDisplayIn = newDisplayIn;
  }
}
